is_simple_core = False

if is_simple_core:
    from flower.core_simple import Variable
    from flower.core_simple import Function
    from flower.core_simple import using_config
    from flower.core_simple import no_grad
    from flower.core_simple import as_array
    from flower.core_simple import as_variable
    from flower.core_simple import setup_variable
else:
    from flower.core import Variable
    from flower.core import Function
    from flower.core import Parameter
    from flower.core import using_config
    from flower.core import no_grad
    from flower.core import test_mode
    from flower.core import as_array
    from flower.core import as_variable
    from flower.core import setup_variable
    from flower.core import Config
    from flower.layers import Layer
    from flower.models import Model
    from flower.dataloaders import DataLoader

    import flower.datasets
    import flower.dataloaders
    import flower.optimizers
    import flower.functions
    import flower.functions_conv
    import flower.layers
    import flower.utils
    import flower.cuda
    import flower.transforms

setup_variable()
