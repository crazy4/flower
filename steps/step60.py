if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from flower import Model
import flower.functions as F
import flower.layers as L
import numpy as np
import flower
import matplotlib.pyplot as plt

max_epoch = 1000
batch_size = 30
hidden_size = 100
bptt_length = 30

train_set = flower.datasets.SinCurve(train=True)
dataloader = flower.dataloaders.SeqDataLoader(train_set, batch_size=batch_size)
seqlen = len(train_set)

class BetterRNN(Model):
    def __init__(self, hidden_size, out_size):
        super().__init__()
        self.rnn = L.LSTM(hidden_size)
        self.fc = L.Linear(out_size)

    def reset_state(self):
        self.rnn.reset_state()

    def forward(self, x):
        y = self.rnn(x)
        y = self.fc(y)
        return y


model = BetterRNN(hidden_size, 1)
optimizer = flower.optimizers.Adam().setup(model)

for epoch in range(max_epoch):
    model.reset_state()
    loss, count = 0, 0

    for x, t in dataloader:
        y = model(x)
        loss += F.mean_squared_error(y, t)
        count += 1

        if count % bptt_length == 0 or count == seqlen:
            model.cleargrads()
            loss.backward()
            loss.unchain_backward()
            optimizer.update()

    avg_loss = float(loss.data.sum()) / count
    print(f"| epoch {epoch+1}| loss {avg_loss:.4f}")
    #print(f"| epoch {epoch+1}")

model.save_weights("e100.npz")

model.load_weights('e100.npz')

xs = np.cos(np.linspace(0, 4 * np.pi, 1000))
model.reset_state()
pred_list =[]

with flower.no_grad():
    for x in xs:
        x = np.array(x).reshape(1, 1)
        y = model(x)
        pred_list.append(float(y.data))

plt.plot(np.arange(len(xs)), xs, label = 'y=cos(x)')
plt.plot(np.arange(len(xs)), pred_list, label = 'predict')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.savefig('sin-predict.pdf')