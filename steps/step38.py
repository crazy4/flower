if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import numpy as np
from flower import Variable
import flower.functions as F

x = Variable(np.array([[1,2,3],[4,5,6]]))
y = F.transpose(x)
y.backward(retain_grad=True)
print(x.grad)
print(y)

x = Variable(np.random.rand(1,2,3))
y = x.reshape((2,3))