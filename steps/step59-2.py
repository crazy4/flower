if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from flower import Model
import flower.functions as F
import flower.layers as L
import numpy as np
import matplotlib.pyplot as plt
import flower

train_set = flower.datasets.SinCurve(train=True)
print(len(train_set))
print(train_set[0])
print(train_set[1])
print(train_set[2])

xs = [example[0] for example in train_set]
ts = [example[0] for example in train_set]

plt.plot(np.arange(len(xs)), xs, label='xs')
plt.plot(np.arange(len(ts)), ts, label='xs')
plt.savefig('sin.png')