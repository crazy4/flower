if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import math
import numpy as np
import flower.functions as F
from flower.models import MLP
from flower import optimizers, DataLoader
from flower.datasets import MNIST
import flower

batch_size = 100
max_epoch = 5
hidden_size = 1000
lr = 1.0

def f(x):
    x = x.flatten()
    x = x.astype(np.float32)
    x /= 255.
    return x

train_set = MNIST(train=True, transform=f)
test_set = MNIST(train=False, transform=f)
train_loader = DataLoader(train_set, batch_size)
test_loader = DataLoader(test_set, batch_size, shuffle=False)

model = MLP((hidden_size, hidden_size, 10), activation=F.relu)
optimizer = optimizers.Adam().setup(model)

for epoch in range(max_epoch):
    sum_loss, sum_acc = 0, 0
    print(f"epoch: {epoch+1}")
    for x, t in train_loader:
        y = model(x)
        loss = F.softmax_cross_entropy(y, t)
        acc = F.accuracy(y, t)
        model.cleargrads()
        loss.backward()
        optimizer.update()

        sum_loss += len(t) * float(loss.data)
        sum_acc += len(t) * float(acc.data)

    print(f"train loss: {sum_loss/len(train_set):.4f}, acc: {sum_acc/len(train_set):.4f}")
    sum_loss, sum_acc = 0, 0

    with flower.no_grad():
        for x, t in test_loader:
            y = model(x)
            loss = F.softmax_cross_entropy(y, t)
            acc = F.accuracy(y, t)

            sum_loss += len(t) * float(loss.data)
            sum_acc += len(t) * float(acc.data)

    print(f"test loss: {sum_loss/len(test_set):.4f}, acc: {sum_acc/len(test_set):.4f}")


# x, t = train_set[0]
# import matplotlib.pyplot as plt
# plt.imshow(x.reshape(28, 28), cmap='gray')
# plt.axis('off')
# plt.show()
# print(f"label: {t}")