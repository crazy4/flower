if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import math
import numpy as np
import flower.functions as F
from flower.models import MLP
from flower import optimizers
import flower.datasets

max_epoch = 300
batch_size = 30
hidden_size = 10
lr = 1.0

x, t = flower.datasets.get_spiral(train=True)
model = MLP((hidden_size, 3))
optimizer = optimizers.SGD(lr).setup(model)

data_size = len(x)
max_iter = math.ceil(data_size / batch_size)

history = []

for epoch in range(max_epoch):
    index = np.random.permutation(data_size)
    sum_loss = 0

    for i in range(max_iter):
        batch_idx = index[i * batch_size:(i + 1) * batch_size]
        batch_x = x[batch_idx]
        batch_t = t[batch_idx]

        y = model(batch_x)
        loss = F.softmax_cross_entropy(y, batch_t)
        model.cleargrads()
        loss.backward()
        optimizer.update()

        sum_loss += float(loss.data) * len(batch_t)

    avg_loss = sum_loss / data_size
    print(f"epoch {epoch+1:.2f}, loss {avg_loss:.2f}")
    history.append(avg_loss)


import matplotlib.pyplot as plt

plt.xlabel('epoch')
plt.ylabel('loss')
plt.plot(history)
plt.show()