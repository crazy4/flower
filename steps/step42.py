if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import numpy as np
from flower import Variable
import flower.functions as F
import matplotlib.pyplot as plt

np.random.seed(0)
xx = np.random.rand(100, 1)
yy = 5 + 2 * xx + np.random.rand(100, 1)
x, y = Variable(xx), Variable(yy)

W = Variable(np.zeros((1,1)))
b = Variable(np.zeros(1))

def predict(x):
    y = F.matmul(x, W) + b
    return y

def mean_squared_error(x0, x1):
    diff = x0 - x1
    return F.sum(diff ** 2) / len(diff)

lr = 0.1
iters = 100

for i in range(iters):
    y_pred = predict(x)
    loss = mean_squared_error(y, y_pred)

    W.cleargrad()
    b.cleargrad()
    loss.backward()

    W.data -= lr * W.grad.data
    b.data -= lr * b.grad.data
    print(W, b, loss)



plt.xlabel("x")
plt.ylabel("y")
plt.plot(xx, yy, "o")
plt.plot(xx, y_pred.data)
plt.show()