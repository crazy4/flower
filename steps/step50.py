if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import math
import numpy as np
import flower.functions as F
from flower.models import MLP
from flower import optimizers, DataLoader
from flower.datasets import Spiral
import flower

max_epoch = 300
batch_size = 30
hidden_size = 10
lr = 1.0

train_set = Spiral(train=True)
test_set = Spiral(train=False)
train_loader = DataLoader(train_set, batch_size)
test_loader = DataLoader(test_set, batch_size, shuffle=False)

model = MLP((hidden_size, 10))
optimizer= optimizers.SGD(lr).setup(model)


train_loss_history, train_acc_history = [], []
test_loss_history, test_acc_history = [], []

for epoch in range(max_epoch):
    sum_loss, sum_acc = 0, 0

    for x, t in train_loader:
        y = model(x)
        loss = F.softmax_cross_entropy(y, t)
        acc = F.accuracy(y, t)
        model.cleargrads()
        loss.backward()
        optimizer.update()

        sum_loss += len(t) * float(loss.data)
        sum_acc += len(t) * float(acc.data)
    print(f"epoch: {epoch+1}")
    print(f"train loss: {sum_loss/len(train_set):.4f}, accuracy: {sum_acc/len(train_set):.4f}")
    train_loss_history.append(sum_loss/len(train_set))
    train_acc_history.append(sum_acc/len(train_set))
    
    sum_loss, sum_acc = 0, 0

    with flower.no_grad():
        for x, t in test_loader:
            y = model(x)
            loss = F.softmax_cross_entropy(y, t)
            acc = F.accuracy(y, t)

            sum_loss += len(t) * float(loss.data)
            sum_acc += len(t) * float(acc.data)
        print(f"epoch: {epoch+1}")
        print(f"test loss: {sum_loss/len(test_set):.4f}, accuracy: {sum_acc/len(test_set):.4f}")
        test_loss_history.append(sum_loss/len(test_set))
        test_acc_history.append(sum_acc/len(test_set))
    

import matplotlib.pyplot as plt

plt.figure(figsize=(10,5))
plt.subplot(1,2,1)
plt.xlabel('epoch')
plt.ylabel('loss')
plt.plot(train_loss_history, label="train")
plt.plot(test_loss_history, label="test")
plt.legend()
plt.subplot(1,2,2)
plt.xlabel('epoch')
plt.ylabel('acc')
plt.plot(train_acc_history, label="train")
plt.plot(test_acc_history, label="test")
plt.legend()
plt.show()