if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import math
import time
import numpy as np
import flower.functions as F
from flower.models import MLP
from flower import optimizers, DataLoader
from flower.datasets import MNIST
import flower

batch_size = 100
max_epoch = 5
hidden_size = 1000
lr = 1.0

def f(x):
    x = x.flatten()
    x = x.astype(np.float32)
    x /= 255.
    return x

train_set = MNIST(train=True)
train_loader = DataLoader(train_set, batch_size)

model = MLP((hidden_size, hidden_size, 10))
optimizer = optimizers.SGD(lr).setup(model)

if flower.cuda.gpu_enable:
    train_loader.to_gpu()
    model.to_gpu()

for epoch in range(max_epoch):
    start = time.time()
    sum_loss = 0

    for x, t in train_loader:
        y = model(x)
        loss = F.softmax_cross_entropy(y, t)
        model.cleargrads()
        loss.backward()
        optimizer.update()

        sum_loss += len(t) * float(loss.data)

    elapsed_time = time.time() - start
    print(f"epoch: {epoch+1}, train loss: {sum_loss/len(train_set):.4f}, time: {elapsed_time:.4f}[sec]")
    sum_loss = 0
