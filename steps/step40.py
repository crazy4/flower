if "__file__" in globals():
    import sys, os

    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import numpy as np
from flower import Variable
import flower.functions as F

x0 = Variable(np.array([1,2,3]))
x1 = Variable(np.array([10]))
y = x0 * x1
print(y)

y.backward()
print(x0.grad, x1.grad)